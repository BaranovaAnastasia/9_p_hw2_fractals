﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Input;
using System.Windows.Controls;

namespace _9_P_HW2_Fractals
{
    /// <summary>
    /// Представляет фракталы типа "Кривая Гильберта"
    /// </summary>
    class HilbertCurve : Fractal
    {
        //float lastX, lastY;
        float totalLength;

        private int xCurrent;
        private int yCurrent;
        private int xNew = 0;
        private int yNew = 0;

        /// <summary>
        /// Инициализирует новый экземпляр класса HilbertCurve
        /// </summary>
        /// <param name="maxRecursionLevel">Уровень рекурсии для отрисовки</param>
        /// <param name="startColor">Цвет для отрисовки элементов первой итерацииparam>
        /// <param name="endColor">Цвет для отрисовки элементов последней итерации</param>
        /// <param name="width">Ширина поля для отрисовки</param>
        /// <param name="height">Высота поля для отрисовки</param>
        public HilbertCurve(int maxRecursionLevel, Color startColor, Color endColor, double width, double height) :
               base((float)((width < height ? width * 0.9 : height * 0.9) / (Math.Pow(2, maxRecursionLevel) - 1)), maxRecursionLevel, startColor, endColor)
        { totalLength = (float)(width < height ? width * 0.9 : height * 0.9); }

        public override void Draw()
        {
            colors = GetGradientColors(endColor, startColor, maxRecursionLevel);
            Canvas.Children.Clear();
            Point start = new Point((Canvas.Width - totalLength) / 2, (Canvas.Height - totalLength) / 2);

            xCurrent = (int)start.X;
            yCurrent = (int)start.Y;
            DrawHilbert();
        }

        // Perform the Drawing
        private void DrawHilbert()
        {
            GenerateHilbertCurve(maxRecursionLevel, 0, (int)firstIterationLength);
        }

        /// <summary>
        /// Метод для рекурсивной отрисовки 
        /// </summary>
        /// <param name="depth">Текущая глубина рекурсии</param>
        /// <param name="xDistance">Длина по X</param>
        /// <param name="yDistance">Длина по Y</param>
        private void GenerateHilbertCurve(int depth, int xDistance, int yDistance)
        {
            if (depth < 1)
                return;
            else
            {
                GenerateHilbertCurve(depth - 1, yDistance, xDistance);

                // Обновляем координаты точек
                FindPointRelative(xDistance, yDistance);
                Line line1 = new Line
                {
                    Stroke = new SolidColorBrush(colors[maxRecursionLevel - depth]),
                    Fill = new SolidColorBrush(colors[maxRecursionLevel - depth]),
                    X1 = xCurrent,
                    X2 = xNew,
                    Y1 = yCurrent,
                    Y2 = yNew,
                    StrokeThickness = 2
                };
                Canvas.Children.Add(line1);
                UpdateCurrentLocation();

                GenerateHilbertCurve(depth - 1, xDistance, yDistance);

                FindPointRelative(yDistance, xDistance);
                Line line2 = new Line
                {
                    Stroke = new SolidColorBrush(colors[maxRecursionLevel - depth]),
                    Fill = new SolidColorBrush(colors[maxRecursionLevel - depth]),
                    X1 = xCurrent,
                    X2 = xNew,
                    Y1 = yCurrent,
                    Y2 = yNew,
                    StrokeThickness = 2
                };
                Canvas.Children.Add(line2);
                UpdateCurrentLocation();

                GenerateHilbertCurve(depth - 1, xDistance, yDistance);

                FindPointRelative(-xDistance, -yDistance);
                Line line3 = new Line
                {
                    Stroke = new SolidColorBrush(colors[maxRecursionLevel - depth]),
                    Fill = new SolidColorBrush(colors[maxRecursionLevel - depth]),
                    X1 = xCurrent,
                    X2 = xNew,
                    Y1 = yCurrent,
                    Y2 = yNew,
                    StrokeThickness = 2
                };
                Canvas.Children.Add(line3);
                UpdateCurrentLocation();

                GenerateHilbertCurve(depth - 1, -yDistance, -xDistance);
            }
        }

        private void FindPointRelative(int xDistance, int yDistance)
        {
            xNew = xCurrent + xDistance;
            yNew = yCurrent + yDistance;
        }

        private void UpdateCurrentLocation()
        {
            xCurrent = xNew;
            yCurrent = yNew;
        }

        protected override void DrawRecursive(double x, double y, double length)
        { }

        public override void Move(Key e, int len)
        {
            if (e == Key.Down)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).Y1 -= len;
                    (Canvas.Children[i] as Line).Y2 -= len;
                }
            if (e == Key.Up)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).Y1 += len;
                    (Canvas.Children[i] as Line).Y2 += len;
                }
            if (e == Key.Left)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).X1 += len;
                    (Canvas.Children[i] as Line).X2 += len;
                }
            if (e == Key.Right)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).X1 -= len;
                    (Canvas.Children[i] as Line).X2 -= len;
                }
        }

        public override void Resize(double ratio)
        {
            for (int i = 0; i < Canvas.Children.Count; i++)
            {
                (Canvas.Children[i] as Line).X2 *= ratio;
                (Canvas.Children[i] as Line).X1 *= ratio;
                (Canvas.Children[i] as Line).Y2 *= ratio;
                (Canvas.Children[i] as Line).Y1 *= ratio;
                (Canvas.Children[i] as Line).StrokeThickness *= ratio;
                Canvas.SetLeft(Canvas.Children[i], Canvas.GetLeft(Canvas.Children[i]) * ratio);
            }
        }
    }
}
