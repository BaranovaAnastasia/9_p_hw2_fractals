﻿using System;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Input;

namespace _9_P_HW2_Fractals
{
    /// <summary>
    /// Представляет фракталы типа "Множество кантора"
    /// </summary>
    public class Cantor : Fractal
    {
        /// <summary>
        /// Коэффициент, отвечающий за расстояние между линиями фрактала
        /// </summary>
        double distance;
        /// <summary>
        /// Получает и задает коэффициент, отвечающий за расстояние между линиями фрактала
        /// </summary>
        public double Distance
        {
            get => distance;
            set
            {
                if (value <= 2)
                    throw new ArgumentException("Distance", "Значение параметра не находилось в границах допустимого диапазона");
                distance = value;
            }
        }

        /// <summary>
        /// Инициализирует новый экземпляр Cantor класса
        /// </summary>
        /// <param name="firstIterationLength">Длина линии на первой итерации</param>
        /// <param name="maxRecursionLevel">Уровень рекурсии для отрисовки</param>
        /// <param name="startColor">Цвет для отрисовки элементов первой итерацииparam>
        /// <param name="endColor">Цвет для отрисовки элементов последней итерации</param>
        /// <param name="dist">Коэффициент, отвечающий за расстояние между линиями фрактала</param>
        public Cantor(double firstIterationLength, int maxRecursionLevel, Color startColor, Color endColor, double dist) :
            base(firstIterationLength, maxRecursionLevel, startColor, endColor)
        {
            if (dist <= 2)
                throw new ArgumentException("dist", "Значение параметра не находилось в границах допустимого диапазона");
            distance = dist;
        }

        public override void Draw()
        {
            colors = GetGradientColors(endColor, startColor, maxRecursionLevel);
            Canvas.Children.Clear();
            DrawRecursive(0, 10, firstIterationLength);
        }

        protected override void DrawRecursive(double x, double y, double length)
        {
            if (maxRecursionLevel - currentRecursionLevel > 1)
            {
                currentRecursionLevel++;
                DrawRecursive(x, y + 20, length / distance);
                if (currentRecursionLevel > 1) DrawRecursive(x + length * (distance - 1), y + 20, length / distance);
                currentRecursionLevel--;
            }

            Line line1 = new Line
            {
                Stroke = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                Fill = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                StrokeThickness = 4,
                X1 = x,
                X2 = x + length,
                Y1 = y,
                Y2 = y
            };
            Canvas.Children.Add(line1);

            if (currentRecursionLevel == 0) return;

            Line line2 = new Line
            {
                Stroke = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                Fill = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                StrokeThickness = 4,
                X1 = x + length * (distance - 1),
                X2 = x + length * distance,
                Y1 = y,
                Y2 = y
            };
            Canvas.Children.Add(line2);
        }

        public override void Move(Key e, int len)
        {
            if (e == Key.Down)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).Y1 -= len;
                    (Canvas.Children[i] as Line).Y2 -= len;
                }
            if (e == Key.Up)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).Y1 += len;
                    (Canvas.Children[i] as Line).Y2 += len;
                }
            if (e == Key.Left)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).X1 += len;
                    (Canvas.Children[i] as Line).X2 += len;
                }
            if (e == Key.Right)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).X1 -= len;
                    (Canvas.Children[i] as Line).X2 -= len;
                }
        }

        public override void Resize(double ratio)
        {
            for (int i = 0; i < Canvas.Children.Count; i++)
            {
                (Canvas.Children[i] as Line).X2 *= ratio;
                (Canvas.Children[i] as Line).X1 *= ratio;
            }
        }
    }
}
