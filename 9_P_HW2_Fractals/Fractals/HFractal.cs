﻿using System.Linq;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace _9_P_HW2_Fractals
{
    class HFractal : Fractal
    {
        /// <summary>
        /// Инициализирует новый экземпляр HFractal класса
        /// </summary>
        /// <param name="maxRecursionLevel">Уровень рекурсии для отрисовки</param>
        /// <param name="startColor">Цвет для отрисовки элементов первой итерацииparam>
        /// <param name="endColor">Цвет для отрисовки элементов последней итерации</param>
        /// <param name="width">Ширина поля для отрисовки</param>
        /// <param name="height">Высота поля для отрисовки</param>
        public HFractal(int maxRecursionLevel, Color startColor, Color endColor, double width, double height) :
            base(width < height ? width / 2 : height / 2, maxRecursionLevel, startColor, endColor)
        {; }

        public override void Draw()
        {
            colors = GetGradientColors(endColor, startColor, maxRecursionLevel);
            Canvas.Children.Clear();
            Point start = new Point(firstIterationLength, firstIterationLength);
            DrawRecursive(start.X, start.Y, firstIterationLength);
        }

        protected override void DrawRecursive(double x, double y, double length)
        {
            if (maxRecursionLevel - currentRecursionLevel > 1)
            {
                currentRecursionLevel++;
                DrawRecursive(x + length / 2, y + length / 2, length / 2);
                DrawRecursive(x - length / 2, y + length / 2, length / 2);
                DrawRecursive(x + length / 2, y - length / 2, length / 2);
                DrawRecursive(x - length / 2, y - length / 2, length / 2);
                currentRecursionLevel--;
            }

            Line line1 = new Line
            {
                Stroke = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                Fill = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                X1 = x - length / 2,
                X2 = x + length / 2,
                Y1 = y,
                Y2 = y
            };
            Canvas.Children.Add(line1);

            Line line2 = new Line
            {
                Stroke = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                Fill = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                X1 = x - length / 2,
                X2 = x - length / 2,
                Y1 = y - length / 2,
                Y2 = y + length / 2
            };
            Canvas.Children.Add(line2);

            Line line3 = new Line
            {
                Stroke = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                Fill = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                X1 = x + length / 2,
                X2 = x + length / 2,
                Y1 = y - length / 2,
                Y2 = y + length / 2
            };
            Canvas.Children.Add(line3);
        }

        public override void Move(Key e, int len)
        {
            if (e == Key.Down)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).Y1 -= len;
                    (Canvas.Children[i] as Line).Y2 -= len;
                }
            if (e == Key.Up)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).Y1 += len;
                    (Canvas.Children[i] as Line).Y2 += len;
                }
            if (e == Key.Left)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).X1 += len;
                    (Canvas.Children[i] as Line).X2 += len;
                }
            if (e == Key.Right)
                for (int i = 0; i < Canvas.Children.Count; i++)
                {
                    (Canvas.Children[i] as Line).X1 -= len;
                    (Canvas.Children[i] as Line).X2 -= len;
                }
        }

        public override void Resize(double ratio)
        {
            for (int i = 0; i < Canvas.Children.Count; i++)
            {
                (Canvas.Children[i] as Line).X2 *= ratio;
                (Canvas.Children[i] as Line).X1 *= ratio;
                (Canvas.Children[i] as Line).Y2 *= ratio;
                (Canvas.Children[i] as Line).Y1 *= ratio;
                (Canvas.Children[i] as Line).StrokeThickness *= ratio;
                Canvas.SetLeft(Canvas.Children[i], Canvas.GetLeft(Canvas.Children[i]) * ratio);
            }
        }
    }
}
