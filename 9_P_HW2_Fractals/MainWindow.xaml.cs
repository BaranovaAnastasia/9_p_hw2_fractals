﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using NLog;

namespace _9_P_HW2_Fractals
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        Fractal currentFractal;
        Color startColor = Colors.Black;
        Color endColor = Colors.Black;

        Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();

        double cantorDistance = 3;

        public MainWindow()
        {
            logger.Info("Application ran");
            InitializeComponent();

            MinHeight = 450;
            MinWidth = 800;
            MaxHeight = SystemParameters.PrimaryScreenHeight;
            MaxWidth = SystemParameters.PrimaryScreenWidth;

            saveFileDialog.FileName = "Fractal"; // Default file name
            saveFileDialog.DefaultExt = ".png"; // Default file extension
            saveFileDialog.Filter = "Bitmap Image (.bmp)|*.bmp|Gif Image (.gif)|*.gif |JPEG Image (.jpeg)|*.jpeg |Png Image (.png)|*.png |Tiff Image (.tiff)|*.tiff"; // Filter files by extension
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
                testcanvas.Width = testcanvas.Height =
                    SystemParameters.PrimaryScreenHeight * 0.83;
            else
                testcanvas.Width = testcanvas.Height =
                    Width > Height ? Height * 0.83 : Width * 0.6;
            if (currentFractal is HilbertCurve)
            {
                currentFractal = new HilbertCurve((int)RecursionLevel.Value, startColor, endColor, testcanvas.Width, testcanvas.Height);
                currentFractal.Canvas = testcanvas;
                currentFractal?.Draw();
            }
            if (currentFractal is HFractal)
            {
                currentFractal = new HFractal((int)RecursionLevel.Value, startColor, endColor, testcanvas.Width, testcanvas.Height);
                currentFractal.Canvas = testcanvas;
                currentFractal?.Draw();
            }
            if (currentFractal is TSquare)
            {
                currentFractal = new TSquare((int)RecursionLevel.Value, startColor, endColor, testcanvas.Width, testcanvas.Height);
                currentFractal.Canvas = testcanvas;
                currentFractal?.Draw();
            }
            if (currentFractal is Cantor)
            {
                currentFractal = new Cantor(testcanvas.Width, (int)RecursionLevel.Value, startColor, endColor, cantorDistance);
                currentFractal.Canvas = testcanvas;
                currentFractal?.Draw();
            }
            logger.Info("Window size successfully changed, fractal has been  redrawn");
        }

        /// <summary>
        /// Обработчик события изменения значения на слайдере
        /// </summary>
        private void RecursionLevel_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Type.SelectedItem == Type.Items[0]) return;
            try
            {
                if (currentFractal is HilbertCurve)
                {
                    currentFractal = new HilbertCurve((int)e.NewValue, startColor, endColor, testcanvas.Width, testcanvas.Height);
                    currentFractal.Canvas = testcanvas;
                    currentFractal.Draw();
                    logger.Info("Recursion level successfully changed, fractal has been redrawn");
                }
                else
                    currentFractal.MaxRecursionLevel = (int)e.NewValue;
            }
            catch (Exception) { logger.Error("Recursion level caused an error"); }
        }

        /// <summary>
        /// Метод для отрисовки Т-Квадрата
        /// </summary>
        private void DrawTSquare()
        {
            RecursionLevel.Maximum = 7;
            logger.Info("Started drawing TSquare");
            currentFractal = new TSquare((int)RecursionLevel.Value, startColor, endColor, testcanvas.Width, testcanvas.Height);
            currentFractal.Canvas = testcanvas;
            currentFractal.MaxRecursionLevelChanged += currentFractal.Draw;
            currentFractal.Draw();
            logger.Info("TSquare has been drawn");
        }

        /// <summary>
        /// Метод для отрисовки Множества Кантора
        /// </summary>
        private void DrawCantor()
        {
            RecursionLevel.Maximum = 10;
            logger.Info("Started drawing Cantor Set");
            currentFractal = new Cantor(testcanvas.Width, (int)RecursionLevel.Value, startColor, endColor, cantorDistance);
            currentFractal.Canvas = testcanvas;
            currentFractal.MaxRecursionLevelChanged += currentFractal.Draw;
            currentFractal.Draw();
            logger.Info("Cantor set has been drawn");
        }

        /// <summary>
        /// Метод для отрисовки Н-Фрактала
        /// </summary>
        private void DrawHFractal()
        {
            RecursionLevel.Maximum = 5;
            logger.Info("Started drawing H-Fractal");
            currentFractal = new HFractal((int)RecursionLevel.Value, startColor, endColor, testcanvas.Width, testcanvas.Height);
            currentFractal.Canvas = testcanvas;
            currentFractal.MaxRecursionLevelChanged += currentFractal.Draw;
            currentFractal.Draw();
            logger.Info("H-Fractal has been drawn");
        }

        /// <summary>
        /// Метод для отрисовки Кривой Гильберта
        /// </summary>
        private void DrawHilbertCurve()
        {
            RecursionLevel.Maximum = 6;
            logger.Info("Started drawing Hilbert Curve");
            currentFractal = new HilbertCurve((int)RecursionLevel.Value, startColor, endColor, testcanvas.Width, testcanvas.Height);
            currentFractal.Canvas = testcanvas;
            currentFractal.MaxRecursionLevelChanged += currentFractal.Draw;
            currentFractal.Draw();
            logger.Info("Hilbert Curve has been drawn");
        }

        /// <summary>
        /// Метод для получения текущего выбранного пользователем фрактала
        /// </summary>
        private void ComboBox_Selected(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            var text = comboBox.SelectedIndex;
            switch (text)
            {
                case 0: testcanvas.Children.Clear(); break;
                case 1:
                    DrawCantor();
                    DistText.Visibility = Visibility.Visible;
                    CantorDistance.Visibility = Visibility.Visible;
                    break;
                case 2: DrawTSquare(); break;
                case 3: DrawHFractal(); break;
                case 4: DrawHilbertCurve(); break;

                default: logger.Error("ComboBox had wrong value"); MessageBox.Show("Unexpected error"); break;
            }
            if (!(currentFractal is Cantor) && DistText != null && CantorDistance != null)
            {
                DistText.Visibility = Visibility.Hidden;
                CantorDistance.Visibility = Visibility.Hidden;
            }
            logger.Info("Fractal type has been changed");
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            Type.IsEnabled = false;
            ClrPcker_Start.IsEnabled = false;
            ClrPcker_End.IsEnabled = false;
            RecursionLevel.IsEnabled = false;

            if (e.KeyboardDevice.Modifiers != ModifierKeys.Control) return;
            if (e.Key == Key.OemPlus)
            {
                currentFractal?.Resize(1.1);
                logger.Info("Fractal resized");
                return;
            }
            if (e.Key == Key.OemMinus)
            {
                currentFractal?.Resize(0.9);
                logger.Info("Fractal resized");
                return;
            }
            if (e.Key == Key.Right || e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down)
            {
                currentFractal?.Move(e.Key, 5);
                logger.Info("Fractal moved");
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl ||
                e.KeyboardDevice.Modifiers != ModifierKeys.Control)
            {
                Type.IsEnabled = true;
                ClrPcker_Start.IsEnabled = true;
                ClrPcker_End.IsEnabled = true;
                RecursionLevel.IsEnabled = true;
            }
        }

        private void ClrPcker_Start_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            startColor = (Color)e.NewValue;
            if (currentFractal == null || Type.SelectedItem == Type.Items[0]) return;
            currentFractal.StartColor = startColor;
            currentFractal.Draw();
            logger.Info("Start color was changed");
        }

        private void ClrPcker_End_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            endColor = (Color)e.NewValue;
            if (currentFractal == null || Type.SelectedItem == Type.Items[0]) return;
            currentFractal.EndColor = endColor;
            currentFractal.Draw();
            logger.Info("End color was changed");
        }

        // Метод для сохранения изображения фрактала
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string filename;
            if (saveFileDialog.ShowDialog() == false) return;
            filename = saveFileDialog.FileName;

            testcanvas.LayoutTransform = null;  //обнуляем маштабировние если было


            //качество изображения
            double dpi = 300;
            double scale = dpi / 96;

            Size size = testcanvas.RenderSize;
            RenderTargetBitmap image;

            if (this.WindowState == WindowState.Maximized)
                image = new RenderTargetBitmap((int)(SystemParameters.PrimaryScreenWidth * scale),
                    (int)(SystemParameters.PrimaryScreenHeight * scale), dpi, dpi, PixelFormats.Pbgra32);
            else
                image = new RenderTargetBitmap((int)(Width * scale), (int)(Height * scale), dpi, dpi, PixelFormats.Pbgra32);
            testcanvas.Measure(size);
            testcanvas.Arrange(new Rect(size)); // This is important      


            image.Render(testcanvas);
            PngBitmapEncoder encoder = new PngBitmapEncoder();  //конвертируем в png формат
            encoder.Frames.Add(BitmapFrame.Create(image));
            try
            {
                using (FileStream file = File.Create(filename))
                {
                    encoder.Save(file);
                }
                System.Drawing.Image image1 = System.Drawing.Bitmap.FromFile(filename);
                System.Drawing.Bitmap bmp = image1 as System.Drawing.Bitmap;
                // Crop the image:
                System.Drawing.Bitmap cropBmp =
                    bmp.Clone(new System.Drawing.Rectangle(840, 65,
                    (int)(testcanvas.Width * 3.2), (int)(testcanvas.Height * 3.2)),
                    bmp.PixelFormat);
                // Release the resources:
                image1.Dispose();
                cropBmp.Save(filename);
                logger.Info("Fractal successfully saved");
            }
            catch (Exception ex)
            {
                logger.Error("Error while saving an image");
                MessageBox.Show("Произошла ошибка при сохранении изображения: " + ex.Message
                    + " Вероятно, изображение не сохранено или сохранено некорректно", "Ошибка",
                    MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        // Обработчик нажатия на клавишу правой стрелки
        private void MoveRight_Click(object sender, RoutedEventArgs e) =>
            currentFractal?.Move(Key.Right, 5);

        // Обработчик нажатия на клавишу левой стрелки
        private void MoveLeft_Click(object sender, RoutedEventArgs e) =>
            currentFractal?.Move(Key.Left, 5);

        // Обработчик нажатия на клавишу стрелки вверх
        private void MoveUp_Click(object sender, RoutedEventArgs e) =>
            currentFractal?.Move(Key.Up, 5);

        // Обработчик нажатия на клавишу стрелки вниз
        private void MoveDown_Click(object sender, RoutedEventArgs e) =>
            currentFractal?.Move(Key.Down, 5);

        private void EnlargeButton_Click(object sender, RoutedEventArgs e) =>
            currentFractal?.Resize(1.1);

        private void ReduceButton_Click(object sender, RoutedEventArgs e) =>
            currentFractal?.Resize(0.9);

        private void CantorDistance_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            cantorDistance = CantorDistance.Value;
            if (currentFractal is Cantor)
            {
                (currentFractal as Cantor).Distance = cantorDistance;
                currentFractal.Draw();
            }
            logger.Info("Cantor Distance coef changed");
        }
    }
}

