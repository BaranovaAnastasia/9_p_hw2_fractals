﻿using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Input;

namespace _9_P_HW2_Fractals
{
    /// <summary>
    /// Представляет фракталы типа "Т-квадрат"
    /// </summary>
    class TSquare : Fractal
    {
        /// <summary>
        /// Инициализирует новый экземпляр TSquare класса
        /// </summary>
        /// <param name="maxRecursionLevel">Уровень рекурсии для отрисовки</param>
        /// <param name="startColor">Цвет для отрисовки элементов первой итерацииparam>
        /// <param name="endColor">Цвет для отрисовки элементов последней итерации</param>
        /// <param name="width">Ширина поля для отрисовки</param>
        /// <param name="height">Высота поля для отрисовки</param>
        public TSquare(int maxRecursionLevel, Color startColor, Color endColor, double width, double height) :
            base(width < height ? width / 2 : height / 2, maxRecursionLevel, startColor, endColor)
        {; }

        public override void Draw()
        {
            colors = GetGradientColors(endColor, startColor, maxRecursionLevel);
            Canvas.Children.Clear();
            Point start = new Point(firstIterationLength - firstIterationLength / 2, firstIterationLength - firstIterationLength / 2);
            DrawRecursive(start.X, start.Y, firstIterationLength);
        }

        protected override void DrawRecursive(double x, double y, double length)
        {
            if (maxRecursionLevel - currentRecursionLevel > 1)
            {
                currentRecursionLevel++;
                DrawRecursive(x + length * 3 / 4, y + length * 3 / 4, length / 2);
                DrawRecursive(x - length / 4, y + length * 3 / 4, length / 2);
                DrawRecursive(x + length * 3 / 4, y - length / 4, length / 2);
                DrawRecursive(x - length / 4, y - length / 4, length / 2);
                currentRecursionLevel--;
            }

            Rectangle rectangle = new Rectangle
            {
                Stroke = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                Fill = new SolidColorBrush(colors[maxRecursionLevel - currentRecursionLevel - 1]),
                Width = length,
                Height = length
            };

            Canvas.SetLeft(rectangle, x);
            Canvas.SetTop(rectangle, y);

            Canvas.Children.Add(rectangle);
        }

        public override void Move(Key e, int len)
        {
            if (e == Key.Down)
                for (int i = 0; i < Canvas.Children.Count; i++)
                    Canvas.SetTop(Canvas.Children[i], Canvas.GetTop(Canvas.Children[i]) - len);
            if (e == Key.Up)
                for (int i = 0; i < Canvas.Children.Count; i++)
                    Canvas.SetTop(Canvas.Children[i], Canvas.GetTop(Canvas.Children[i]) + len);
            if (e == Key.Left)
                for (int i = 0; i < Canvas.Children.Count; i++)
                    Canvas.SetLeft(Canvas.Children[i], Canvas.GetLeft(Canvas.Children[i]) + len);
            if (e == Key.Right)
                for (int i = 0; i < Canvas.Children.Count; i++)
                    Canvas.SetLeft(Canvas.Children[i], Canvas.GetLeft(Canvas.Children[i]) - len);
        }

        public override void Resize(double ratio)
        {
            for (int i = 0; i < Canvas.Children.Count; i++)
            {
                (Canvas.Children[i] as Rectangle).Width =
                         (Canvas.Children[i] as Rectangle).Height =
                          (Canvas.Children[i] as Rectangle).Width * ratio;
                Canvas.SetLeft(Canvas.Children[i], Canvas.GetLeft(Canvas.Children[i]) * ratio);
                Canvas.SetTop(Canvas.Children[i], Canvas.GetTop(Canvas.Children[i]) * ratio);
            }
        }
    }
}