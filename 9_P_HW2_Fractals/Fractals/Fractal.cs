﻿using System;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Input;

namespace _9_P_HW2_Fractals
{
    public abstract class Fractal
    {
        protected double firstIterationLength;
        protected int currentRecursionLevel = 0;
        protected int maxRecursionLevel;

        protected Color startColor;
        protected Color endColor;
        protected Color[] colors;

        /// <summary>
        /// Происходит при изменении максимального уровня рекурсии
        /// </summary>
        public event Action MaxRecursionLevelChanged;

        /// <summary>
        /// Канвас, на котором произходит отрисовка
        /// </summary>
        public Canvas Canvas = new Canvas();

        /// <summary>
        /// Получает и задает максимальный уровень рекурсии для данного фрактала
        /// </summary>
        public int MaxRecursionLevel
        {
            get => maxRecursionLevel;
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("Error");
                maxRecursionLevel = value;
                MaxRecursionLevelChanged?.Invoke();
            }
        }

        /// <summary>
        /// Получает и задает начальный цвет градиента
        /// </summary>
        public Color StartColor
        {
            get => startColor;
            set =>
                startColor = value;
        }

        /// <summary>
        /// Получает и задает конечный цвет градиента
        /// </summary>
        public Color EndColor
        {
            get => endColor;
            set =>
                endColor = value;
        }

        /// <summary>
        /// Инициализирует новый экземпляр Fractal класса 
        /// </summary>
        /// <param name="firstIterationLength">Длина отрезка (сторона квадрата, треугольника,
        /// радиус окружности и т.д.) на первой итерации</param>
        /// <param name="maxRecursionLevel">Уровень рекурсии для отрисовки</param>
        /// <param name="startColor">Цвет для отрисовки элементов первой итерации</param>
        /// <param name="endColor">Цвет для отрисовки элементов последней итерации</param>
        protected Fractal(double firstIterationLength, int maxRecursionLevel, Color startColor, Color endColor)
        {
            if (firstIterationLength <= 0)
                throw new ArgumentOutOfRangeException("firstIterationLength",
                    "Длина отрезка должна быть положительной");

            if (maxRecursionLevel <= 0)
                throw new ArgumentOutOfRangeException("maxRecursionLevel",
                    "Уровень рекурсии должен быть положительным");

            this.firstIterationLength = firstIterationLength;
            this.maxRecursionLevel = maxRecursionLevel;
            this.startColor = startColor;
            this.endColor = endColor;
        }

        /// <summary>
        /// Запускает рекурсивную отрисовку фрактала на канвасе
        /// </summary>
        public abstract void Draw();

        /// <summary>
        /// Рекурсивно рисует данный фрактал
        /// </summary>
        /// <param name="x">X координата точки, в которой начитается отрисовка на данной итерации</param>
        /// <param name="y">Y координата точки, в которой начитается отрисовка на данной итерации</param>
        /// <param name="length">Длина отрезка (сторона квадрата, треугольника,
        /// радиус окружности и т.д.) на текущей итерации</param>
        protected abstract void DrawRecursive(double x, double y, double length);

        /// <summary>
        /// Двигает фрактал на канвасе в указанном направлении на указанное расстояние 
        /// </summary>
        /// <param name="e">Клавиша, определяющая направление движения</param>
        /// <param name="len">Расстояние</param>
        public abstract void Move(Key e, int len);

        /// <summary>
        /// Увеличивает изображение фрактала
        /// </summary>
        /// <param name="ratio">Знаменатель</param>
        public abstract void Resize(double ratio);

        /// <summary>
        /// Возвращает массив цветов для всех итеращий отрисовки с использованием линейного градиента
        /// </summary>
        /// <param name="start">Цвет на первой итерации</param>
        /// <param name="end">Цвет на последней итерации</param>
        /// <param name="steps">Количество итераций</param>
        protected static Color[] GetGradientColors(Color start, Color end, int steps)
        {
            Color[] colors = new Color[steps];
            colors[0] = start;
            colors[steps - 1] = end;

            double aStep = (end.A - start.A) / steps;
            double rStep = (end.R - start.R) / steps;
            double gStep = (end.G - start.G) / steps;
            double bStep = (end.B - start.B) / steps;

            for (int i = 1; i < steps - 1; i++)
            {
                var a = start.A + (aStep * i);
                var r = start.R + (rStep * i);
                var g = start.G + (gStep * i);
                var b = start.B + (bStep * i);
                colors[i] = Color.FromArgb((byte)a, (byte)r, (byte)g, (byte)b);
            }
            return colors;
        }
    }
}
